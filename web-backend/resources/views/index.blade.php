<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <title>A-BANK</title>
    <link href="{{ URL::asset('dist_template/css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('dist_template/css/lib/calendar2/semantic.ui.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('dist_template/css/lib/calendar2/pignose.calendar.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('dist_template/css/lib/owl.carousel.min.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('dist_template/css/lib/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('dist_template/css/helper.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('dist_template/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('dist_template/css/lib/sweetalert/sweetalert.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('dist_template/css/lib/html5-editor/bootstrap-wysihtml5.css')}}" />


</head>

<body class="fix-header fix-sidebar">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <div id="main-wrapper">
            <div class="header">
                <nav class="navbar top-navbar navbar-expand-md navbar-light">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/">
                            A-BANK
                        </a>
                    </div>
                    <div class="navbar-collapse">
                        <ul class="navbar-nav mr-auto mt-md-0">
                            <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                            <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="left-sidebar">
                <div class="scroll-sidebar">
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav">
                            <li class="nav-devider"></li>
                            <li class="nav-label">Home</li>
                            <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Master </span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/bank">Bank</a></li>
                                    <li><a href="/admin/layanan">Layanan</a></li>
                                    <li><a href="/admin/staff">Staff</a></li>
                                </ul>
                            </li>
                            <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Permohonan </span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="/admin/antrian">Proses Permohonan</a></li>
                                    <li><a href="#">Laporan Permohonan</a></li>
                                </ul>
                            </li>
                            <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Config </span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="#">Ganti Password</a></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="page-wrapper">
                @yield('content')
                <footer class="footer"> © 2018 A-BANK. Designed by <a href="https://himatif.ubpkarawang.ac.id.com">STRUGGLE TEAM</a></footer>
            </div>
        </div>
        <script src="{{ URL::asset('dist_template/js/lib/jquery/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/sidebarmenu.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/morris-chart/raphael-min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/morris-chart/morris.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/morris-chart/dashboard1-init.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/calendar-2/moment.latest.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/calendar-2/semantic.ui.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/calendar-2/prism.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/calendar-2/pignose.calendar.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/calendar-2/pignose.init.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/owl-carousel/owl.carousel-init.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/scripts.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/custom.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/sweetalert/sweetalert.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/sweetalert/sweetalert.init.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/custom.min.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/html5-editor/wysihtml5-0.3.0.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/html5-editor/bootstrap-wysihtml5.js')}}"></script>
        <script src="{{ URL::asset('dist_template/js/lib/html5-editor/wysihtml5-init.js')}}"></script>
    </body>
    </html>