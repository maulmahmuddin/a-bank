@extends('index')
@section('content')
<script type="text/javascript" src="{{ URL::asset('js/qrcode.js')}}"></script>
<style type="text/css">
#qrcode {
  width:160px;
  height:160px;
  margin: 0;
  background: red;
}
</style>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-4">
                <a href="/antrian">
                    <div class="card bg-primary p-20">
                        <div class="media widget-ten">
                            <div class="media-left meida media-middle">
                                <span><i class="ti-bag f-s-40"></i></span>
                            </div>
                            <div class="media-body media-text-right">
                                <h2 class="color-white">{{$model['service']->name}}</h2>
                                <p class="m-b-0">{{$model['service']->description}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <br><br><br>
                                <h1 class="text-center">
                                    {{$model['bank']->name}}<br>
                                    <div class="text-center" style="font-size: 14px;">{{$model['bank']->address}}</div>
                                    <div class="text-center" style="font-size: 12px; color: red;">* SCAN qr code ini untuk verifikasi antrian anda</div>
                                </h1>
                            </div>
                            <div class="col-md-6">
                                <div id="qrcode" style="width:100px; height:100px; margin-top:15px;"></div>

                            </div>                            
                        </div>
                        <br><br>
                        <div class="card-two">
                            <h3>A-BANK ( Antrian bank online )</h3>
                            <div class="desc">
                                2018
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <script type="text/javascript">
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width : 250,
            height : 250,
            margin : 0
        });

        function makeCode () {      
            var bank = {!! $model['bank'] !!}
            var service = {!! $model['service'] !!}
            var qrId = +bank['id'] +'-'+service['id'];
            qrId = qrId+"-"+getFormattedDate();

            console.log(qrId);
            qrcode.makeCode(qrId);
        }

        function getFormattedDate() {
            var date = new Date();
            var str = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" +  date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();

            return str;
        }

        function refreshData() {
            x = 5;
            makeCode();
            setTimeout(refreshData, x*1000);
        }

        refreshData();
    </script>
    @endsection