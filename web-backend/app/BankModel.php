<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankModel extends Model
{
    protected $table = "tm_bank";
    protected $primaryKey = "id";
    public $timestamps = false;
}
