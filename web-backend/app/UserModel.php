<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = "tm_user";
    protected $primaryKey = "id";
    public $timestamps = false;
}
