<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminModel extends Model
{
    protected $table = "tm_staff";
    protected $primaryKey = "id";
    public $timestamps = false;
}
