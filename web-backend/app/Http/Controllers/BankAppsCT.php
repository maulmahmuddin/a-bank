<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserModel;
use App\BankModel;
use App\ServiceModel;
use App\TrxBookingModel;
use App\AdminModel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BankAppsCT extends Controller
{

    public function index(Request $request)
    {
        $data = $request->session()->get('user_data');

        $bank = new BankModel();
        $bank = $bank->where('id', $data->bank_id)->first();

        $service = new ServiceModel();
        $service = $service->where('id', $data->service_id)->first();

        $model['user'] = $data;
        $model['bank'] = $bank;
        $model['service'] = $service;

        return view('home', compact('model'));
    }

    public function loginUI()
    {
        return view('login');
    }

    public function check(Request $request)
    {
        $data = $request->session()->get('user_data');

        if($data != null){
            return redirect()->to('dashboard');
        }else{
            return redirect()->to('login');
        }
    }

    public function login(Request $request)
    {

        $userid = $request->user_id;
        $password = $request->password;

        $model = new AdminModel();
        $model = $model->where('username', $userid)->where('password', $password)->first();

        if($model != null){

            $bank = new BankModel();
            $bank = $bank->where('id', $model->bank_id)->first();

            session(['user_data' => $model]);
            session(['bank_data' => $bank]);
            return redirect()->to('dashboard');
        }else{
            return redirect()->to('login')->with('alert-danger', 'User ID atau Password salah.');
        }
    }

    public function bookingRequest(Request $request)
    {
        $data = $request->session()->get('user_data');

        $dateNow = Carbon::now()->toDateString();

        $bank = new BankModel();
        $bank = $bank->where('id', $data->bank_id)->first();

        $service = new ServiceModel();
        $service = $service->where('id', $data->service_id)->first();

        $dataBank = DB::table('tr_booking')
        ->select('tr_booking.*', 'tm_bank.name as bank_name', 'tm_bank.address as bank_address', 'tm_service.name as service_name', 'tm_service.description as service_description', DB::raw('TIME_FORMAT(time, "%H:%i") as time'), DB::raw('DATE_FORMAT(date, "%d %M %Y") as date'), DB::raw('TIME_FORMAT(TIMEDIFF(time, curtime()), "%H` j %i` min") as diff'))
        ->join('tm_bank', 'tr_booking.bank_id', '=', 'tm_bank.id')
        ->join('tm_service', 'tr_booking.service_id', '=', 'tm_service.id')
        ->where(['bank_id' => $data->bank_id, 'service_id' => $data->service_id, 'date' => $dateNow, 'tr_booking.status' => '0'])
        ->first();

        if($data != null){

            $dataList = DB::table('tr_booking')
            ->select('tr_booking.*', 'tm_bank.name as bank_name', 'tm_bank.address as bank_address', 'tm_service.name as service_name', 'tm_service.description as service_description', DB::raw('TIME_FORMAT(time, "%H:%i") as time'), DB::raw('DATE_FORMAT(date, "%d %M %Y") as date'), DB::raw('TIME_FORMAT(TIMEDIFF(time, curtime()), "%H` j %i` min") as diff'))
            ->join('tm_bank', 'tr_booking.bank_id', '=', 'tm_bank.id')
            ->join('tm_service', 'tr_booking.service_id', '=', 'tm_service.id')
            ->where(['bank_id' => $data->bank_id, 'service_id' => $data->service_id, 'date' => $dateNow])
            ->get();
            $model['dataList'] = $dataList;
        }

        $model['user'] = $data;
        $model['bank'] = $bank;
        $model['service'] = $service;
        $model['data'] = $dataBank;

        // return $model;

        return view('booking.list', compact('model'));
    }

    public function bookingRequestDetail($id, Request $request)
    {
        $data = $request->session()->get('user_data');

        $dateNow = Carbon::now()->toDateString();

        $bank = new BankModel();
        $bank = $bank->where('id', $data->bank_id)->first();

        $service = new ServiceModel();
        $service = $service->where('id', $data->service_id)->first();

        $data = DB::table('tr_booking')
        ->select('tr_booking.*', 'tm_bank.name as bank_name', 'tm_bank.address as bank_address', 'tm_service.name as service_name', 'tm_service.description as service_description', DB::raw('TIME_FORMAT(time, "%H:%i") as time'), DB::raw('DATE_FORMAT(date, "%d %M %Y") as date'), DB::raw('TIME_FORMAT(TIMEDIFF(time, curtime()), "%H` j %i` min") as diff'))
        ->join('tm_bank', 'tr_booking.bank_id', '=', 'tm_bank.id')
        ->join('tm_service', 'tr_booking.service_id', '=', 'tm_service.id')
        ->where(['tr_booking.id' => $id])
        ->first();

        $model['user'] = $data;
        $model['bank'] = $bank;
        $model['service'] = $service;
        $model['data'] = $data;

        $trxBookingModel = new TrxBookingModel();
        $trxBookingModel = $trxBookingModel->where('id', $id)->first();
        $trxBookingModel->status = 2;
        $trxBookingModel->start_time = Carbon::now()->format('H:i');
        $trxBookingModel->save();

        // return $model;

        return view('booking.service', compact('model'));
    }

    

    public function skipBooking($id, Request $request)
    {

        $trxBookingModel = new TrxBookingModel();
        $trxBookingModel = $trxBookingModel->where('id', $id)->first();
        $trxBookingModel->status = 3;
        $trxBookingModel->save();

        return redirect()->to('antrian');
    }

    public function doneBooking($id, Request $request)
    {

        $trxBookingModel = new TrxBookingModel();
        $trxBookingModel = $trxBookingModel->where('id', $id)->first();
        $trxBookingModel->status = 1;
        $trxBookingModel->end_time = Carbon::now()->format('H:i');
        $trxBookingModel->save();

        return redirect()->to('antrian');
    }

    public function logout(Request $request)
    {

        $request->session()->forget('user_data');
        $request->session()->forget('bank_data');
        return redirect()->to('login');
    }

    
}
