## Copyright and license

Copyright (C) UBP Struggle Team, Inc - All Rights Reserved
#### UNAUTHORIZED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED. PROPRIETARY AND CONFIDENTIAL.

Written by UBP Struggle Team, July 2018

### This means you can't:
- Copy the file
- Print the file out, scan it and copy the image
- Print the file out, take pictures of it and distribute the film
- etc 

This source just for validation to the originals source code of SELISIK Competition